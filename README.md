VNC
=========

A role that installs and configures tigerVNC server on targeted host(s) for a single user

Requirements
------------

None

Role Variables
--------------

- vnc_user: User to configure VNC server systemd service for
- create_new_user: Toggle whether to create the user because it does not already exist
- vnc_passwd: Password to set as the VNC password
- user_password: Password hash to set for the vnc_user if the user is created

Dependencies
------------


Example Playbook
----------------

Installing VNC server for default new user bob

    - hosts: servers
      roles:
         - role: vnc

License
-------

MIT

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
